<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//A
	'activer_hashtags_sur' => 'Activer les hashtags sur @objet@',

	//C
	//'titre_champs' => 'Champs',

	//E
	'err_champs_et_groupe_obligatoire' => 'Il faut choisir au moins un champ et un groupe de mots-clés.',
	'err_activer_utilisation_mots_cles' => 'Afin de configurer le plugin hastags, vous devez d\'abord activer la gestion des groupes de mots-clés sur la
											<a href="@url1@">page de configuration des contenus du site</a> et
											parametrer les l\'association des groupe de mot-clefs sur la <a href="@url2@">page de configuration des mots-clés</a>.',

	//I
	'info_formulaire_configurer_hashtags' => 'Parmi les objets listés ci-dessous, veuillez sélectionner les champs sur lesquels sera utilisée la syntaxe #hashtags. Pour chaque objet choisi, sélectionnez au moins un champ et un groupe de mots-clés.' ,

	//T
	'titre_configurer_hashtags' => 'Configurer les hashtags',

	//V
	'verifier_configuration_des_objets' => 'Veuillez vérifier la configuration des objets ; vous devez devez choisir au moins un champ et un groupe de mots-clés pour chaque objet activé.',

);